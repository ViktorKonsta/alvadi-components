class Loading
	constructor: ->
		@loadingScope = $('.loading')
	init: ->
		@loadingScope.addClass 'show'
	end: ->
		@loadingScope.removeClass 'show'

module.exports = -> new Loading