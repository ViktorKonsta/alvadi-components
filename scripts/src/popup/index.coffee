Utils = require '../utils'

class Popup
	constructor: (options) ->
		if not Utils.there options.scope then return 

		{@scope, selector, close, init} = options

		@body = $ 'body'

		if init then init options

		if selector
			selector.on 'click', (e) => do @open
		else
			do @open

		closeBtn = @scope.find close

		closeBtn.on 'click', (e) => do @close

		@scope.on 'click', (e) ->
			if e.target is $(@)[0] then closeBtn.trigger 'click'

	open: ->
		@scope.addClass 'show-popup'
		@body.addClass 'no-scroll'

	close: ->
		@scope.removeClass 'show-popup'
		@body.removeClass 'no-scroll'

module.exports = (options) -> new Popup options