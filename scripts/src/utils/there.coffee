###*
	* This function check if element that we
	* need in this page. If not then return 'false'.
	* It can be useful to avoid useless execution.
	@param {jQuery} element - element to find 
###
module.exports = (element) ->
	if $('body').find(element)[0] then return yes
	return no