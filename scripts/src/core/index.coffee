module.exports = class Core
	log: (message) -> console.log message
	debug: (message) -> console.debug message
	info: (message) -> console.info message
	error: (message) -> console.error new Error message