
/**
	* This function check the checkbox if it's
	* not checked yet. And disable if it's checked.
	* Return input
	@param {jQuery} Object - input to check
 */
module.exports = function(input) {
  if (input.is(':checked')) {
    input.removeAttr('checked');
    return input;
  }
  input.attr('checked', '');
  return input;
};
