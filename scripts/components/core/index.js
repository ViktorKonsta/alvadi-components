var Core;

module.exports = Core = (function() {
  function Core() {}

  Core.prototype.log = function(message) {
    return console.log(message);
  };

  Core.prototype.debug = function(message) {
    return console.debug(message);
  };

  Core.prototype.info = function(message) {
    return console.info(message);
  };

  Core.prototype.error = function(message) {
    return console.error(new Error(message));
  };

  return Core;

})();
