var Popup, Utils;

Utils = require('../utils');

Popup = (function() {
  function Popup(options) {
    var close, closeBtn, init, selector;
    if (!Utils.there(options.scope)) {
      return;
    }
    this.scope = options.scope, selector = options.selector, close = options.close, init = options.init;
    this.body = $('body');
    if (init) {
      init(options);
    }
    if (selector) {
      selector.on('click', (function(_this) {
        return function(e) {
          return _this.open();
        };
      })(this));
    } else {
      this.open();
    }
    closeBtn = this.scope.find(close);
    closeBtn.on('click', (function(_this) {
      return function(e) {
        return _this.close();
      };
    })(this));
    this.scope.on('click', function(e) {
      if (e.target === $(this)[0]) {
        return closeBtn.trigger('click');
      }
    });
  }

  Popup.prototype.open = function() {
    this.scope.addClass('show-popup');
    return this.body.addClass('no-scroll');
  };

  Popup.prototype.close = function() {
    this.scope.removeClass('show-popup');
    return this.body.removeClass('no-scroll');
  };

  return Popup;

})();

module.exports = function(options) {
  return new Popup(options);
};
