var Loading;

Loading = (function() {
  function Loading() {
    this.loadingScope = $('.loading');
  }

  Loading.prototype.init = function() {
    return this.loadingScope.addClass('show');
  };

  Loading.prototype.end = function() {
    return this.loadingScope.removeClass('show');
  };

  return Loading;

})();

module.exports = function() {
  return new Loading;
};
