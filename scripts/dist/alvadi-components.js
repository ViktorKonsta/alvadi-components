(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = window.alvadi = {
  popup: require('./popup'),
  loading: require('./loading'),
  Utils: require('./utils')
};

},{"./loading":2,"./popup":3,"./utils":4}],2:[function(require,module,exports){
var Loading;

Loading = (function() {
  function Loading() {
    this.loadingScope = $('.loading');
  }

  Loading.prototype.init = function() {
    return this.loadingScope.addClass('show');
  };

  Loading.prototype.end = function() {
    return this.loadingScope.removeClass('show');
  };

  return Loading;

})();

module.exports = function() {
  return new Loading;
};

},{}],3:[function(require,module,exports){
var Popup, Utils;

Utils = require('../utils');

Popup = (function() {
  function Popup(options) {
    var close, closeBtn, init, selector;
    if (!Utils.there(options.scope)) {
      return;
    }
    this.scope = options.scope, selector = options.selector, close = options.close, init = options.init;
    this.body = $('body');
    if (init) {
      init(options);
    }
    if (selector) {
      selector.on('click', (function(_this) {
        return function(e) {
          return _this.open();
        };
      })(this));
    } else {
      this.open();
    }
    closeBtn = this.scope.find(close);
    closeBtn.on('click', (function(_this) {
      return function(e) {
        return _this.close();
      };
    })(this));
    this.scope.on('click', function(e) {
      if (e.target === $(this)[0]) {
        return closeBtn.trigger('click');
      }
    });
  }

  Popup.prototype.open = function() {
    this.scope.addClass('show-popup');
    return this.body.addClass('no-scroll');
  };

  Popup.prototype.close = function() {
    this.scope.removeClass('show-popup');
    return this.body.removeClass('no-scroll');
  };

  return Popup;

})();

module.exports = function(options) {
  return new Popup(options);
};

},{"../utils":4}],4:[function(require,module,exports){
module.exports = {
  there: require('./there'),
  toggleCheck: require('./toggle-check')
};

},{"./there":5,"./toggle-check":6}],5:[function(require,module,exports){

/**
	* This function check if element that we
	* need in this page. If not then return 'false'.
	* It can be useful to avoid useless execution.
	@param {jQuery} element - element to find
 */
module.exports = function(element) {
  if ($('body').find(element)[0]) {
    return true;
  }
  return false;
};

},{}],6:[function(require,module,exports){

/**
	* This function check the checkbox if it's
	* not checked yet. And disable if it's checked.
	* Return input
	@param {jQuery} Object - input to check
 */
module.exports = function(input) {
  if (input.is(':checked')) {
    input.removeAttr('checked');
    return input;
  }
  input.attr('checked', '');
  return input;
};

},{}]},{},[1]);
