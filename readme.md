# Alvadi-components
**Front-end Alvadi Framework**  
**For building ambitious Alvadi services**

### About

It's developing to be response for most front-end parts in all Alvadi projects.  
It should be used with *browserify* for managing at least scripts-part.

Each module will be packed to the other modules so we will get two files in the output
only. That's it. Two files.

In the end we have few things to return

1. Object with modules return
2. Styles return
	- all styles, 
3. Framework assets return

### Structure

`components/` - actuall bootstrap and modules of the library  
`lib/` - some different helpful scripts  
`src-tests/` - source of the tests that imitate the project  
`tasks/` - tasks to *src-tests*  

### Why?

* avoid fuckups/repeatitions/compatibility issues
* make you don't forget about all features you have
* implement Object-Oriented code style into scripts *(powered by Coffee & browserify)*
* make scripts the easiest to *rw* things you have ever

### How to get into?

From anywhere you are, first you want to install that package in `/node_modules`  
via *npm* and register it as *dependency* in *package.json*.

Imagine you need to implement a new popup window to whatever. To realise that you go  
to your scripts folder and write something realy easy and straightforward.

```coffee
Popup = require('alvadi-components').Popup
someNewAwesomePopup = new Popup()
```

And there we go. We have a nice Alvadi-styled full working version of popup.  
Then you should only provide some elements, callbacks and... cool, huh?

### Why not bootstrap/foundation/whatever?

That Frameworks are awesome and contain all things we need - yes. But, they have  
too much, that mess of features are'nt in structure, we can't controll that. The  
second reason is we need to style all that things again and again. And at least  
wee need fully controlled custom API pretty much.

### What is done?

### Popup

```js
alvadi.popup({
	scope: $('.popup-some-alert')
	selector: $('button#trigger-popup')
	close: $('button#close-btn')
	init: function(selector) {
		alert('popup is opened');
		console.log(selector)
	}
});
```

**`scope`**  
**обязательно**

Этот элемент является границей popup. От него будет произведён поиск всех остальных
указанных элементов. Кроме того, именно по наличию этого объекта на странице будет
решаться - продолжать или останавливать скрипт.

**`close`**  
**обязательно**

Элемент кнопки "закрыть".

**`selector`**  
**необязательно**

Элемент по нажатию на который сработает popup. Если не указан - сработает сразу же.

**`init`**  
**необязательно)**

Функция, которая будет вызвана перед тем как сработает popup.

### ToggleCheck

```js
alvadi.Utils.toggleCheck($('input[type="checkbox"]'))
```