# ==============================\

# Define

# ==============================/

$ = require('auto-require')()

# ==============================\

# Configuration

# ==============================/

cfg =
	scripts:
		components: "scripts/components"
		dist: "scripts/dist"
		src: "scripts/src"
	styles:
		src: "styles/src"
		dist: "styles/dist"

# ==============================\

# Build scripts

# ==============================/

$.gulp.task 'build:components', ->
	$.gulp.src "#{cfg.scripts.src}/**/*.coffee"
		.pipe do $.plumber
		.pipe $.coffee bare: on
		.pipe $.gulp.dest "#{cfg.scripts.components}/"

$.gulp.task 'build:dist', ->
	$.gulp.src "#{cfg.scripts.src}/index.coffee"
		.pipe do $.plumber
		.pipe do $.coffeeify
		.pipe $.rename 'alvadi-components.js'
		.pipe $.gulp.dest "#{cfg.scripts.dist}/"

$.gulp.task 'build', ['build:components', 'build:dist']

# ==============================\

# Build styles

# ==============================/

rucksackBundle =
	$.rucksackCss
		autoprefixer: on
		fallbacks: on

$.gulp.task 'styles', ->
	$.gulp.src "#{cfg.styles.src}/index.styl"
		.pipe do $.plumber
		.pipe $.stylus
			'include css': on
			use: [ $.poststylus [rucksackBundle] ]
		.pipe do $.csso
		.pipe $.rename 'alvadi-components.css'
		.pipe $.gulp.dest "#{cfg.styles.dist}/"

# ==============================\

# Watch task && Default task

# ==============================/

$.gulp.task 'watch', ->
	$.watch "#{cfg.scripts.src}/**/*.coffee", -> $.gulp.start 'build'
	$.watch "#{cfg.styles.src}/**/*.styl", -> $.gulp.start 'styles'

$.gulp.task 'default', ['build', 'styles', 'watch']